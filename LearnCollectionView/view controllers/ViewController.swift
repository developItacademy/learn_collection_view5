//
//  ViewController.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit
import CloudKit
import UserNotifications

enum NetworkError: Error {
    case domainError
    case decodingError
    case generalError
}


protocol WorkWithStudentArray {
    var users: [User] { get set}
    func getNextStudent(pointerToCurrentStudent: Int) -> User
    func getStudentLocationInArray(student: User) -> Int
}

class ViewController: UIViewController, WorkWithStudentArray {
    
    // FIXME: Need to determine how we alert next student
    func getNextStudent(pointerToCurrentStudent: Int) -> User {
        let nbr = Int.random(in: 1..<dataForApp.students.count)
        return dataForApp.students[nbr]
    }
    
    func getStudentLocationInArray(student: User) -> Int {
        let locationInArray = dataForApp.students.firstIndex { (usr) -> Bool in
            usr.username == student.username
        }
        return locationInArray!
    }
    
    
    // MARK: - properties of the ViewController
    var dbs : CKDatabase {
        return CKContainer(identifier: "iCloud.com.dia.cloudKitExample.open").publicCloudDatabase
    }
    
    var users: [User] = [User]()
    var ipadID : String = ""
    var uuid: String = ""
    
    var udidFromCfgFile: String = ""
    var assetTagFromCfgFile: String = ""
    
    var groupNbrForiPad: String = ""
    
    var dataForApp : DataForApp =  DataForApp()
    
    @IBOutlet var msgToSend: UITextField?
    @IBOutlet weak var collectionItems: UICollectionView!
 
    
    
    // MARK: - View controller life cycle functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let paths = collectionItems.indexPathsForSelectedItems {
            for path in paths {
                collectionItems.deselectItem(at: path, animated: true)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchAppConfiguration()
        
        navigationItem.title =  AppData.categories[groupNbrForiPad]
        
        setIpadIdentifier()
        
        GetStudents.getStudentFromJamfSchool(fromWhichGroupNumber: self.groupNbrForiPad) { students in
            students.forEach { (student) in
                print(student.firstName, student.notes)
                self.dataForApp.studentUsernames.append(student.username)
            }
            self.dataForApp.students = students
            self.configureAndReloadCollectionView()
        }
    }
        
    func fetchAppConfiguration() {
        
        
        
        guard ipadID.isEmpty else {
            return
        }
        
        
        // rc9
//        let udidMng  = "0e07a9e3d8d0a2a0c74710504960441a52ce8044"
//        let assetMng  = "21"
        
        

        guard let managedConfigDict = UserDefaults.standard.object(forKey: "com.apple.configuration.managed") else {
            displayAlertFailedGettingManagedConfig(with: "failed reading the managed config file with the proper key")
            return
        }

        guard let managedConfigDictDict = managedConfigDict as?  [String:Any?]  else {
            displayAlertFailedGettingManagedConfig(with: "Failed converting it to dictionary")
            return
        }

        guard let udidMng  = managedConfigDictDict["udid"] as?  String  else {
            displayAlertFailedGettingManagedConfig(with: "failed to get udid")
            return
        }

        guard let assetMng  = managedConfigDictDict["asset"] as?  String  else {
            displayAlertFailedGettingManagedConfig(with: "failed to get asset ")
            return
        }

        ipadID = udidMng
        groupNbrForiPad = assetMng
        
        // FIXME: this is just for debugging
       // displayAlertFailedGettingManagedConfig(with: "the ipad udid is \(ipadID) and the group number is \(groupNbrForiPad)")
        
    }

    fileprivate func displayAlertFailedGettingManagedConfig(with message: String) {
        let alertVC = UIAlertController(title: "Failed in the managed configuration process", message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        self.present(alertVC, animated: true)
    }

    
    
    // MARK: - Helper Functions
    
    
    
    
    func setIpadIdentifier()  {
  
        print("```in getting setIpadIdentifier")
        guard let uuid = UIDevice.current.identifierForVendor?.uuidString  else { fatalError("Fatal Error: can't get the iPad ID") }
        self.uuid = uuid
        print("```uuid",uuid)
            
    }
    
    func configureAndReloadCollectionView()  {
        
        self.collectionItems.dataSource = self
        self.collectionItems.delegate = self
        
        self.collectionItems.backgroundColor = UIColor.white
        
        let layout = self.collectionItems.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 25, left: 20, bottom: 25, right: 20)
        
        self.collectionItems.reloadData()
        
    }
    
    func getRandomStudent() -> User {
        let number = Int.random(in: 0 ..< dataForApp.students.count)
        return dataForApp.students[number]
    }
    
//    func displayalert() {
//        DispatchQueue.main.async {
//
//            let alertVC = UIAlertController(title: "Enter Info", message: "Need to input iPad info", preferredStyle: .alert)
//            alertVC.addTextField()
//            alertVC.addTextField()
//
//            let msg = UIAlertAction(title: "Enter UUID and group no", style: .default)  { [unowned alertVC] _ in
//                let udid = alertVC.textFields![0].text!
//                let grpNo = alertVC.textFields![1].text!
//                // do something interesting with "answer" here
//                self.createiPadRecord(passedUDID: udid, groupNbr: grpNo)
//            }
//            alertVC.addAction(msg)
//
//            self.present(alertVC, animated: true)
//    }
    
//    func createiPadRecord(passedUDID: String, groupNbr: String) {
//
//        /// Instantiate a CKRecord with the CKRecordID
//
//        let id = CKRecord.ID(recordName: uuid)
//        let record = CKRecord(recordType: "MyInfo", recordID: id)
//
//
//        /// populate the 2 fields
//        record["currentUser"] = groupNbr as NSString
//        record["name"] = passedUDID as NSString
//
//
//        /// save it
//        dbs.save(record) { [unowned self] (record, error) in
//            print("```* - * - Saving New iPad . . .")
//            DispatchQueue.main.async {
//                if let error = error {
//                    print("```* - * - error saving it \(error)")
//                } else {
//                    print("```* - * - succesful ***")
//                    print(record as Any)
//
//                    let iPadid = CKRecord.ID(recordName: passedUDID)
//                    let iPadrecord = CKRecord(recordType: "iPad", recordID: iPadid)
//
//
//                    /// populate the 1 fields
//                    iPadrecord["identifier"] = self.uuid as NSString
//                    self.dbs.save(iPadrecord) { [unowned self] (record, error) in
//                        print("```* - * - Saving New iPad . . .")
//                        DispatchQueue.main.async {
//                            if let error = error {
//                                print("```* - * - error saving it \(error)")
//                            } else {
//                                print("```* - * - succesful ***")
//                                print(record as Any)
//
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//    }
    
    
    
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate  {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionItems.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as? BookCell else {fatalError("could not deque")}
//        cell.bookCover.image = UIImage(named: AppData.items[indexPath.section][indexPath.row])
        // cell.bookCover.image = UIImage(named: AppData.items[indexPath.row])
        cell.bookCover.image = UIImage(named: dataForApp.students[indexPath.row].username)
       return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataForApp.students.count
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailVC = segue.destination as? DetailViewController else {
            fatalError("Can not segua to detail view controller")
        }
        guard let ip = collectionItems.indexPathsForSelectedItems?.first else {fatalError()}
        print(collectionItems.indexPathsForSelectedItems?.first)
        // detailVC.theItem = AppData.items[ip.row]
        detailVC.theStudent = dataForApp.students[ip.row]
        detailVC.thePointerInArray = ip.row
        detailVC.ipadID = ipadID
        detailVC.uuid = uuid
        detailVC.workWithStudentArrayDelegate = self
    }
    
    
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        return false
//    }
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        switch kind {
//        case UICollectionView.elementKindSectionHeader :
//            let headerView = collectionItems.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "myHeader", for: indexPath) as! HeaderReusableView
//             headerView.label.text = AppData.categories[groupNbrForiPad]
////            headerView.label.text = udidFromCfgFile + " " + assetTagFromCfgFile
//            headerView.image.image = UIImage(named: "gradientTop")
//            return headerView
//        case UICollectionView.elementKindSectionFooter :
//            let footerView = collectionItems.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "myFooter", for: indexPath) as! FooterReusableView
//            footerView.image.image = UIImage(named: "gradientBottom")
//            return footerView
//        default:
//            fatalError("header footer viiew failed")
//        }
//    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
//    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
//        return false
//    }
    
    
}


