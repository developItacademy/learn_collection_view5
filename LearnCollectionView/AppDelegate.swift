//
//  AppDelegate.swift
//  LearnCollectionView
//
//  Created by Steven Hertz on 8/15/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        /// The default group that will be used.
        UserDefaulltsHelper.registerUserDefaults()
        
        // TODO:  Need to put the process of saving the group as a user default in its own process
        // UserDefaulltsHelper.writeGroupUserDefaults(withGroupNumber: "9")

        let notificationcenter = UNUserNotificationCenter.current()
        
        notificationcenter.requestAuthorization(options: [.alert, .sound, .badge], completionHandler: { (granted, error) in
            if granted && error == nil {
                let main = OperationQueue.main
                main.addOperation({
                    notificationcenter.delegate = self
                })
            }
        })

        
        return true
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    /// Asks the delegate how to handle a notification that arrived while the app was running in the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    /// Asks the delegate to process the user's response to a delivered notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        /// remove sent or scheduled to be sent notifications
        center.removeDeliveredNotifications(withIdentifiers: ["nextStudent"])
        center.removePendingNotificationRequests(withIdentifiers: ["nextStudent"])

        let body = response.notification.request.content.body
        print("`````this is the content of the notification \(body)")
        
        let action = response.actionIdentifier
        print(action)
        
        // get the next student
        let userInfo = response.notification.request.content.userInfo
        let studentData = userInfo["name"] as! Data
        let decoder = JSONDecoder()
        guard let student = try? decoder.decode(User.self, from: studentData) else {fatalError("could not decode")}
        print(student.firstName)

        // get the DetailVC
        let navigationVC = window?.rootViewController as! UINavigationController
        let topVC = navigationVC.topViewController as! DetailViewController

        // populate the student info of the DetailViewcontroller
        topVC.theStudent = student
        topVC.studentImageView.image = UIImage(named: student.username)
        // topVC.theItem = student.username
        
        completionHandler()
    }

}
